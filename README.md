
[![build status](https://gitlab.com/bitfireAT/vcard4android/badges/master/build.svg)](https://gitlab.com/bitfireAT/vcard4android/commits/master)


# vcard4android

vcard4android is an Android library that brings together VCard and Android.
It's a framework for

* parsing and generating VCard resources (using [ez-vcard](https://github.com/mangstadt/ez-vcard))
  from/into data classes that are compatible with the Android Contacts Provider, and
* accessing the Android Contacts Provider by a unified API.

It has been primarily developed for [DAVdroid](https://www.davdroid.com).

Generated KDoc: https://bitfireAT.gitlab.io/vcard4android/dokka/vcard4android/


## License 

Copyright (C) bitfire web engineering (Ricki Hirner, Bernhard Stockmann).

This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome
to redistribute it under the conditions of the [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).

